package br.com.empresasjava.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.empresasjava.datasource.model.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long>{
	public Optional<Movie> findByName(String movieName);
}
