package br.com.empresasjava.resource.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MovieResource {
	
	@JsonProperty("name")
	private String name;
	@JsonProperty("genre")
	private String genre;
	@JsonProperty("release")
	private String release;
	@JsonProperty("rating")
	private String rating;
	@JsonProperty("votes")
	private String votes;
	
	public MovieResource(String name, String genre, String release, String rating, String votes) {
		super();
		this.name = name;
		this.genre = genre;
		this.release = release;
		this.rating = rating;
		this.votes = votes;
	}
	
	public MovieResource() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getRelease() {
		return release;
	}

	public void setRelease(String release) {
		this.release = release;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getVotes() {
		return votes;
	}

	public void setVotes(String votes) {
		this.votes = votes;
	}

	@Override
	public String toString() {
		return "MovieResource [name=" + name + ", genre=" + genre + ", release=" + release + ", rating=" + rating
				+ ", votes=" + votes + "]";
	}
}
