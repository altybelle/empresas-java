package br.com.empresasjava.resource.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RateResource {
	@JsonProperty("rating")
	private String rating;

	public RateResource(String rating) {
		super();
		this.rating = rating;
	}
	
	public RateResource() {
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "VoteResource [rating=" + rating + "]";
	}
}
