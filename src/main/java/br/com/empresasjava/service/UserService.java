package br.com.empresasjava.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.empresasjava.datasource.model.User;
import br.com.empresasjava.exception.UserNotFoundException;
import br.com.empresasjava.exception.UserResourceException;
import br.com.empresasjava.repository.UserRepository;
import br.com.empresasjava.resource.model.UserResource;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserConverter userConverter;
	
	public List<User> listAllUsers() {
		List<User> userList = userRepository.findAll();
		return userList;
	}

	public User findUserById(Long userId) throws UserNotFoundException {
			Optional<User> userOptional = getOptional(userId);
			User user = null;
			if (!userOptional.isPresent()) {
				throw new UserNotFoundException("User not found by the ID: " + userId);
			} else {
				user = userOptional.get();
			}
		return user;
	}
	
	public User findUserByUsername(String username) throws UserNotFoundException {
		Optional<User> userOptional = userRepository.findByUsername(username);
		User user = null;
		if (!userOptional.isPresent()) {
			throw new UserNotFoundException("User not found by the username: " + username);
		} else {
			user = userOptional.get();
		}
		return user;
	}

	public void saveUser(UserResource userResource) {
		try {
			User user = userConverter.converter(userResource);
			userRepository.saveAndFlush(user);
		} catch (UserResourceException e) {
			e.printStackTrace();
		}
		
	}

	public void editUser(Long userId, UserResource userResource) throws UserNotFoundException {
		try {
			Optional<User> userOptional = getOptional(userId);
			if(!userOptional.isPresent()) {
				throw new UserNotFoundException("User not found by the ID: " + userId);
			} else {
				User user = userConverter.converter(userResource);
				if (user.getUsername() != null)
					userOptional.get().setUsername(user.getUsername());
				if (user.getPassword() != null)
					userOptional.get().setPassword(user.getPassword());
				userOptional.get().setModifiedAt(new Date(System.currentTimeMillis()));
				userRepository.save(userOptional.get());
			}
		} catch (UserResourceException e) {
			e.printStackTrace();
		}
	}

	public void deleteUser(Long userId) throws UserNotFoundException {
		Optional<User> userOptional = getOptional(userId);
		if (!userOptional.isPresent()) {
			throw new UserNotFoundException("");
		} else {
			userRepository.deleteById(userOptional.get().getId());
		}
		
	}
	
	private Optional<User> getOptional(Long userId) {
		Optional<User> userOptional = userRepository.findById(userId);
		return userOptional;
	}

}
