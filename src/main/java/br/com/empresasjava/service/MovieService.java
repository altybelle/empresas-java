package br.com.empresasjava.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.empresasjava.datasource.model.Movie;
import br.com.empresasjava.exception.MovieNotFoundException;
import br.com.empresasjava.exception.MovieResourceException;
import br.com.empresasjava.repository.MovieRepository;
import br.com.empresasjava.resource.model.MovieResource;
import br.com.empresasjava.resource.model.RateResource;

@Service
public class MovieService {
	
	@Autowired
	private MovieRepository movieRepository;
	@Autowired
	private MovieConverter movieConverter;
	
	public List<Movie> listAllMovies() {
		List<Movie> movieList = movieRepository.findAll();
		return movieList;
	}
	
	public Movie findMovieById(Long movieId) throws MovieNotFoundException {
		Optional<Movie> movieOptional = getOptional(movieId);
		Movie movie = null;
		if (!movieOptional.isPresent()) {
			throw new MovieNotFoundException("Movie not found by the ID: " + movieId);
		} else {
			movie = movieOptional.get();
		}
		return movie;
	}
	
	public Movie findMovieByName(String movieName) throws MovieNotFoundException {
		Optional<Movie> movieOptional = movieRepository.findByName(movieName);
		Movie movie = null;
		if (!movieOptional.isPresent()) {
			throw new MovieNotFoundException("Movie not found by the name: " + movieName);
		} else {
			movie = movieOptional.get();
		}
		return movie;
	}
	
	public void saveMovie(MovieResource movieResource) {
		try {
			Movie movie = movieConverter.converter(movieResource);
			movieRepository.saveAndFlush(movie);
		}  catch (MovieResourceException e) {
			e.printStackTrace();
		}
	}
	
	public void rateMovie(Long movieId, RateResource rateResource) throws MovieNotFoundException {
		Optional<Movie> movieOptional = getOptional(movieId);
		if (!movieOptional.isPresent()) {
			throw new MovieNotFoundException("Movie not found by the ID: " + movieId);
		} else {
			Integer rating = Integer.parseInt(rateResource.getRating());
			movieOptional.get().setVotes(movieOptional.get().getVotes() + 1);
			movieOptional.get().setRating((movieOptional.get().getRating() + rating) / movieOptional.get().getVotes());
			movieRepository.save(movieOptional.get());
		}
	}
	
	public void editMovie(Long movieId, MovieResource movieResource) throws MovieNotFoundException {
		try {
			Optional<Movie> movieOptional = getOptional(movieId);
			if (!movieOptional.isPresent()) {
				throw new MovieNotFoundException("Movie not found by the ID: " + movieId);
			} else {
				Movie movie = movieConverter.converter(movieResource);
				movieOptional.get().setName(movie.getName());
				movieOptional.get().setGenre(movie.getGenre());
				movieOptional.get().setRelease(movie.getRelease());
				movieOptional.get().setRating(movie.getRating());
				movieOptional.get().setVotes(movie.getVotes());
				movieRepository.save(movieOptional.get());
			}
		} catch (MovieResourceException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteMovie(Long movieId) throws MovieNotFoundException {
		Optional<Movie> movieOptional = getOptional(movieId);
		if (!movieOptional.isPresent()) {
			throw new MovieNotFoundException("Movie not found by the ID: " + movieId);
		} else {
			movieRepository.deleteById(movieOptional.get().getId());
		}
	}
	
	private Optional<Movie> getOptional(Long movieId) {
		Optional<Movie> movieOptional = movieRepository.findById(movieId);
		return movieOptional;
	}
}
