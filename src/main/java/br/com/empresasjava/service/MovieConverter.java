package br.com.empresasjava.service;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import br.com.empresasjava.datasource.model.Movie;
import br.com.empresasjava.exception.MovieResourceException;
import br.com.empresasjava.resource.model.MovieResource;

@Component
public class MovieConverter {
	public Movie converter(MovieResource movieResource) throws MovieResourceException {
		try {
			Movie movie = new Movie();
			
			LocalDate movieRelease = checkMovieRelease(movieResource.getRelease());
			
			movie.setName(movieResource.getName());
			movie.setGenre(movieResource.getGenre());
			movie.setRelease(movieRelease);
			movie.setRating(0F);
			movie.setVotes(0);
			
			return movie;
		} catch (Exception e) {
			throw new MovieResourceException("Failed to convert a resource to an entity. Resource: " + movieResource);
		}
	}
	
	@SuppressWarnings("unused")
	private LocalDate checkMovieRelease(String movieRelease) {
		return LocalDate.parse(movieRelease);
	}
}
