package br.com.empresasjava.service;

import java.util.Date;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import br.com.empresasjava.datasource.model.User;
import br.com.empresasjava.exception.UserResourceException;
import br.com.empresasjava.resource.model.UserResource;

@Component
public class UserConverter {

	public User converter(UserResource userResource) throws UserResourceException {
		try {
			User user = new User();
			user.setUsername(userResource.getUsername());
			user.setEmail(userResource.getEmail());
			user.setPassword(new BCryptPasswordEncoder().encode(userResource.getPassword()));
			user.setCreatedAt(new Date(System.currentTimeMillis()));
			user.setModifiedAt(new Date(System.currentTimeMillis()));
			
			return user;
		} catch (Exception e) {
			throw new UserResourceException("Failed to convert a UserResource to an entity. Resource: " + userResource);
		}
	}
}
