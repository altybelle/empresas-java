package br.com.empresasjava.exception;

public class MovieResourceException extends Exception {
	
	private static final long serialVersionUID = -4368517546442165227L;

	public MovieResourceException() {
		super();
	}

	public MovieResourceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MovieResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public MovieResourceException(String message) {
		super(message);
	}

	public MovieResourceException(Throwable cause) {
		super(cause);
	}
}
