package br.com.empresasjava.exception;

public class UserResourceException extends Exception {

	private static final long serialVersionUID = -2814417915797249245L;

	public UserResourceException() {
		super();
	}

	public UserResourceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UserResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserResourceException(String message) {
		super(message);
	}

	public UserResourceException(Throwable cause) {
		super(cause);
	}
}
