package br.com.empresasjava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresasjava.datasource.model.User;
import br.com.empresasjava.exception.UserNotFoundException;
import br.com.empresasjava.resource.model.UserResource;
import br.com.empresasjava.service.UserService;
import io.swagger.annotations.ApiOperation;

@RestController @RequestMapping(value = "v1/user")
public class UserController {
	@Autowired
	private UserService userService;
	
	@GetMapping(path = "/list")
	@ApiOperation(value = "Returns a list with all users.", response = User[].class)
	public List<User> listAllUsers() {
		return userService.listAllUsers();
	}
	
	@GetMapping(path = "/find-id/{id}")
	@ApiOperation(value = "Returns a user given its id.", response = User.class)
	public User findUserById(@PathVariable(name = "id", required = true) Long userId) throws UserNotFoundException {
		return userService.findUserById(userId);
	}
	
	@GetMapping(path = "/find-username/{username}")
	@ApiOperation(value = "Returns a user given its username.", response = User.class)
	public User findUserByEmail(@PathVariable(name = "username", required = true) String username) throws UserNotFoundException {
		return userService.findUserByUsername(username);
	}
	
	@PostMapping(path = "/create")
	@ApiOperation(value = "Saves a new user in the database.")
	public void saveUser(@RequestBody UserResource userResource) {
		userService.saveUser(userResource);
	}
	
	@PutMapping(path = "/edit/{id}")
	@ApiOperation(value = "Edits movies information.")
	public void editUser(@PathVariable(name = "id", required = true) Long userId, @RequestBody UserResource userResource) throws UserNotFoundException {
		userService.editUser(userId, userResource);
	}
	
	@DeleteMapping(path = "/delete/{id}")
	@ApiOperation(value = "Deletes a movie from the database.")
	public void deleteUser(@PathVariable(name = "id", required = true) Long userId) throws UserNotFoundException {
		userService.deleteUser(userId);
	}
}
