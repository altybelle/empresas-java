package br.com.empresasjava.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresasjava.datasource.model.Movie;
import br.com.empresasjava.exception.MovieNotFoundException;
import br.com.empresasjava.resource.model.MovieResource;
import br.com.empresasjava.resource.model.RateResource;
import br.com.empresasjava.service.MovieService;
import io.swagger.annotations.ApiOperation;

@RestController @RequestMapping(value = "/v1/movie")
public class MovieController {
	@Autowired
	private MovieService movieService;
	
	@GetMapping(path = "/list")
	@ApiOperation(value = "Returns a list with all movies.", response = Movie[].class)
	public List<Movie> listAllMovies() {
		return movieService.listAllMovies();
	}
	
	@GetMapping(path = "/find-id/{id}")
	@ApiOperation(value = "Returns a movie given its id.", response = Movie.class)
	public Movie findMovieById(@PathVariable(name = "id", required = true) Long movieId) throws MovieNotFoundException {
		return movieService.findMovieById(movieId);
	}
	
	@GetMapping(path = "/find-name/{name}")
	@ApiOperation(value = "Returns a movie given its name.", response = Movie.class)
	public Movie findMovieByName(@PathVariable(name = "name", required = true) String movieName) throws MovieNotFoundException {
		return movieService.findMovieByName(movieName);
	}
	
	@PostMapping(path = "/create")
	@ApiOperation(value = "Saves a new movie in the database.")
	public void saveMovie(@RequestBody MovieResource movieResource) {
		movieService.saveMovie(movieResource);
	}
	
	@PutMapping(path = "/rate/{id}")
	@ApiOperation(value = "Rates a movie.")
	public void rateMovie(@PathVariable(name = "id", required = true) Long movieId, @RequestBody RateResource rateResource) throws MovieNotFoundException {
		movieService.rateMovie(movieId, rateResource);
	}
	
	@PutMapping(path = "/edit/{id}")
	@ApiOperation(value = "Edits movies information.")
	public void editMovie(@PathVariable(name = "id", required = true) Long movieId, @RequestBody MovieResource movieResource) throws MovieNotFoundException {
		movieService.editMovie(movieId, movieResource);
	}
	
	@DeleteMapping(path = "/delete/{id}")
	@ApiOperation(value = "Deletes a movie from the database.")
	public void deleteMovie(@PathVariable(name = "id", required = true) Long movieId) throws MovieNotFoundException {
		movieService.deleteMovie(movieId);
	}
}
