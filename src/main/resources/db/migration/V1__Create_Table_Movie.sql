create table `movie` (
	`id` bigint not null,
	`genre` varchar(255) not null,
	`name` varchar(255) not null,
	`rating` float not null,
	`release` date not null,
	`votes` integer not null,
	primary key (`id`)
) engine=InnoDB
