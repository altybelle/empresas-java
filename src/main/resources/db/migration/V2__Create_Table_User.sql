create table `user` (
	`id` bigint not null, 
	`created_at` datetime(6) not null, 
	`email` varchar(255) not null, 
	`modified_at` datetime(6) not null, 
	`password` varchar(255) not null, 
	primary key (`id`)
) engine=InnoDB